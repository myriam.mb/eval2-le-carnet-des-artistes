@extends('template')

@section('titre')
    Le carnet des artistes
@stop

@section('header')
    <img src="" alt="Logo">
    <button>Deconnexion</button>
@stop

@section('president')
    <p>Dates et Lieux</p>
@stop

@section('message')
    <form method="POST" action="{{ route('subjects') }}" accept-charset="UTF-8">
        @csrf
        <label for="nom">Proposer un sujet</label>
        <input type="text" name="nom" id="nom" size="20">
        <button type="submit" class="btn btn-warning" style="width: 250px;">Envoyer</button>
    </form>
@stop
@section('contenu')
    
@stop


