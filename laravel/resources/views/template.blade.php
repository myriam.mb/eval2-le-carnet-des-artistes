<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <!-- Scripts -->
    <script src="{{ asset('./js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('./css/app.css') }}" rel="stylesheet">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <title>@yield('titre')</title>
</head>
<body>
    <header>
        @yield('header')
    </header>

    <section>
        @yield('president')
    </section>

    <section>
        @yield('message')
    </section>

    <section>
        @yield('contenu')
    </section>

    <footer>
    </footer>
</body>
</html>