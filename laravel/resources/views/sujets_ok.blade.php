@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
            <div class="col-sm-offset-3 col-sm-6 row justify-content-center">
				<div class="panel panel-info">
				<div class="panel-heading">
					<h3>Merci. Sujet envoyé.</h3>
				</div>
				<div class="">
				<a href="{{ route('home') }}" class="btn btn-secondary" role="button">Envoyer un autre sujet</a>
			</div>
			</div>
	</div>
        </div>
    </div>
   
</div>
@endsection
