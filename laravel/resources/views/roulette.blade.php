@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           
                @foreach ($sujets as $sujet)
                @if($sujet->status)
                <h3 class="row justify-content-center card-title">Le sujet est : </h3>
                <div class="card body">
                    <h3 class="row justify-content-center card-title">{{$sujet->sujet}}</h3>
                </div>
                <h3 class="row justify-content-center card-title">Sujet {{$sujet->id}}</h3>
                @endif
            @endforeach
          
        </div>
    </div>
</div>
@endsection
