@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        </div>
    </div>
    <section>
        <div class="row justify-content-center">
            <div>
                @foreach ($battles as $battle)
                    <h3 class="">La prochaine battle est le : </h3>
                    <h3 class="">{{$battle->date}}</h3>
                    <h3 class="">Elle se déroule à {{$battle->place}}</h3>
                @endforeach
            </div>
        </div> 
    </section>
   
</div>
@endsection
