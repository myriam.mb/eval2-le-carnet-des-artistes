@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="">
                <!-- <div class="card-header">{{ __('Dashboard') }}</div> -->
                <form method="POST" action="{{ route('sujets_ok') }}" accept-charset="UTF-8" class="row justify-content-center">
                    @csrf
                    <label for="nom" class="form-label">Proposer un sujet</label>
                    <input type="text" name="input" id="input" size="20" class="form-control">
                    <button type="submit" class="btn btn-secondary">Envoyer</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
