@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row justify-content-center">
                <form method="POST" action="{{ route('battle_ok') }}" accept-charset="UTF-8" class="row justify-content-center">
                    @csrf
                    <label for="nom" class="form-label">Date</label>
                    <input type="text" name="date" id="date" size="15" class="form-control">
                    <label for="nom" class="form-label">Lieu</label>
                    <input type="text" name="place" id="place" size="20" class="form-control">
                    <button type="submit" class="btn btn-secondary">Envoyer</button>
                </form>
            </div>
        </div>
    </div>
    
</div>
@endsection
