<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Page du site
Route::get('/homeUser','HomeUserController@accueil');


// //Pour le formulaire
Route::get('sujets_ok', [App\Http\Controllers\SujetsController::class, 'show'])->name('sujets_ok');
Route::post('sujets_ok', [App\Http\Controllers\SujetsController::class, 'save'])->name('sujets_ok');

// Pour la roulette
Route::get('roulette', [App\Http\Controllers\RoulettesController::class, 'roule'])->name('roulette');

// Pour la date
Route::get('battle', [App\Http\Controllers\BattleController::class, 'show'])->name('battle');
Route::get('battle_ok', [App\Http\Controllers\BattleController::class, 'afficheBattle'])->name('battle_ok');
Route::get('battle', [App\Http\Controllers\BattleController::class, 'roule'])->name('battle');
Route::post('battle_ok', [App\Http\Controllers\BattleController::class, 'save'])->name('battle_ok');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);
});

Route::get('roulette', [App\Http\Controllers\SujetsController::class, 'afficheRoulette'])->name('roulette');
