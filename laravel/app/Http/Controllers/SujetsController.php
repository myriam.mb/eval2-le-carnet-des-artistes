<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Sujet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SujetsController extends Controller
{

    public function show_(Sujet $sujet)
    {
      return view("sujets.sujet", compact("sujet"));
    }

    public function afficheRoulette(){
        return view('roulette', ['sujets' => Sujet::all()->where('status')->random(1)
        ]);
    }

    public function show(){
        return view('sujets_ok');
    }

    public function save(Request $request){
        DB::table('sujets')->insert([
            'sujet' => $request->input('input'),
            'status'=> true,
        ]);
        return view('sujets_ok');
    }
}
