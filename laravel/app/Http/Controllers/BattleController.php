<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Battle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BattleController extends Controller
{
    //

    public function roule(){
        return view('battle');
    }

    public function show_(Battle $battle)
    {
      return view("battles.battle", compact("battle"));
    }

    public function afficheBattle(){
        return view('battle_ok', ['battles' => Battle::all()->random(1)
        ]);
    }

    public function show(){
        return view('battle');
    }

    public function save(Request $request){
        DB::table('battles')->insert([
            'date' => $request->input('date'),
            'place'=> $request->input('place'),
        ]);
        return view('battle_ok');
    }
}
